#!/bin/bash

# config tsd.storage.hbase.zk_quorum
if [ -n "$ZK_QUORUM" ]; then
    sed -i "s#ZK_QUORUM#$ZK_QUORUM#" /etc/opentsdb/opentsdb.conf
else
    echo "[ERROR] $(date +'%Y-%m-%d %H:%M%S') $HOSTNAME \$ZK_QUORUM is null"  
    exit 1
fi

echo "[INFO] $(date +'%Y-%m-%d %H:%M%S') $HOSTNAME Wait $WAIT_HOSTS"
/wait

exec gosu opentsdb /usr/share/opentsdb/bin/tsdb tsd --config=/etc/opentsdb/opentsdb.conf
