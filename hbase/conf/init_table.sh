#!/bin/bash

# Get current directory
BASE_FOLDER=$(cd "$(dirname "$0")"; pwd)

# wait 60s
sleep 60

echo "[INFO] $(date +'%Y-%m-%d %H:%M%S') $HOSTNAME wait 70 background execution creates the table."
env COMPRESSION=none HBASE_HOME=$HBASE_HOME ${BASE_FOLDER}/create_table.sh
