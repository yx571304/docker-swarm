### docker hbase

    version: 1.2.12
    container os: debian 10 → openjdk:8-jre-slim → hbase:1.2.12

### 构建镜像

    make build

### 上传镜像

> 修改 Makefile 文件中的前4行替换为自己的仓库地址及项目名称

    make push

### 测试(docker-compose)

> 替换镜像地址

    make image

> 创建数据目录

    mkdir -p /swarm/volumes/zk-1
    mkdir -p /swarm/volumes/hadoop-hdfs-nn
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-1

> 启动容器

    make start

### 验证

> 浏览器打开URL 验证

    http://192.168.2.76:16010
    http://192.168.2.76:16030

### docker stack

> 部署

    docker stack deploy -c hbase.yml cluster

> 查看运行状态

    docker service ls --filter name=cluster_hbase
    docker stack ps cluster --filter name=cluster_hbase

> 删除

    docker service rm $(docker service ls --filter name=cluster_hbase --format {{.Name}})
