+++
title = "Caddy Web Server"
+++

### Caddy web server custom build

Plugins:

* http.ipfilter
* http.prometheus

### 监控

**[portainer:9000](http://192.168.0.71:9000)**

**[prometheus:9090](http://192.168.0.71:9090)**

**[grafana:3000](http://192.168.0.71:3000)**

### 服务web管理


**[hadoop-hdfs-nn:50070](http://192.168.0.71:50070)**

**[hadoop-hdfs-dn-1:50075](http://192.168.0.71:50075)**

**[hadoop-hdfs-dn-2:50075](http://192.168.0.71:50076)**

**[hadoop-hdfs-dn-3:50075](http://192.168.0.71:50077)**

**[hadoop-yarn-rm:8088](http://192.168.0.71:8088)**

**[hadoop-yarn-nm:8042](http://192.168.0.71:8042)**
