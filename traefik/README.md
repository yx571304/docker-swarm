### docker stack

> 部署

    docker stack deploy -c traefik.yml cluster

> 查看运行状态 浏览器打开 http://宿主机IP:8080 访问

    docker service ls --filter name=cluster_traefik
    docker stack ps cluster --filter name=cluster_traefik

> 删除

    docker service rm $(docker service ls --filter name=cluster_traefik --format {{.Name}})
