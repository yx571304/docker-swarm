### 下载镜像/更改TAG/上传到私有镜像仓库

    make

### 测试(docker-compose)

> 启动容器

    make start

> 停止

    make stop

### docker stack

> 创建数据目录

    mkdir -p /swarm/volumes/zk-1
    mkdir -p /swarm/volumes/zk-2
    mkdir -p /swarm/volumes/zk-3

> 部署

    docker stack deploy -c zookeeper.yml cluster

> 查看运行状态

    docker service ls --filter name=cluster_zk
    docker stack ps cluster --filter name=cluster_zk

> 删除

    docker service rm $(docker service ls --filter name=cluster_zk --format {{.Name}})
