### docker hadooop

    此镜像基于 debian 10 包含 gosu: (hadoop用户启动服务)  wait: (等待依赖服务启动)

    version: 2.7.7
    container os: debian 10 → openjdk:8-jre-slim → hadoop:2.7.7

### 构建镜像

    make build

### 上传镜像

> 修改 Makefile 文件中的前4行替换为自己的仓库地址及项目名称

    make push

### 测试(docker-compose)

> 创建数据目录

    mkdir -p /swarm/volumes/hadoop-hdfs-nn
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-1
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-2
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-3

> 启动容器

    make start

> 增加副本

    docker-compose -f hadoop-compose.yml scale hadoop-yarn-nm=3

### 验证

> 浏览器打开URL 验证

    http://192.168.2.76:50070
    http://192.168.2.76:8088
    http://192.168.2.76:8042

### docker stack

> 创建数据目录

    mkdir -p /swarm/volumes/hadoop-hdfs-nn
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-1
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-2
    mkdir -p /swarm/volumes/hadoop-hdfs-dn-3

> 部署

    docker stack deploy -c hadoop.yml cluster

> 查看运行状态

    docker service ls --filter name=cluster_hadoop
    docker stack ps cluster --filter name=cluster_hadoop

> 删除

    docker service rm $(docker service ls --filter name=cluster_hadoop --format {{.Name}})

### 说明

```shell
  hadoop-yarn-rm:                          # 服务名称集群内以此名称通信
    image: hadoop:2.7.7                    # 镜像
    restart: always                        # 退出后自动重启
    hostname: hadoop-yarn-rm               # 容器主机名 
    container_name: hadoop-yarn-rm         # 容器名称
    ports:                                 # 映射端口号  宿主机:容器
      - 8088:8088
    environment:                           # 环境变量
      TZ: Asia/Shanghai
      WAIT_HOSTS: hadoop-hdfs-nn:50070     # 等待 hadoop-hdfs-nn 服务启动完成后再启动
      WAIT_AFTER_HOSTS: 5
      WAIT_HOSTS_TIMEOUT: 120
    command:
      - /opt/hadoop-config/bootstrap.sh    # 容器启动命令
      - resourcemanager
    healthcheck:                           # 容器健康检查
      test: curl --head http://127.0.0.1:8088 || exit 1
      interval: 30s
      timeout: 10s
      retries: 3
      start_period: 20s
```
