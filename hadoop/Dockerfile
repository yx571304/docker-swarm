FROM openjdk:8-jre-slim

MAINTAINER xiongjunfeng@haocang.com    

ENV TZ=Asia/Shanghai \
    HADOOP_VER=2.7.7 \
    HADOOP_HOME=/home/hadoop \
    HADOOP_LOG_DIR=/home/hadoop/logs

ENV HADOOP_PREFIX=${HADOOP_HOME} \
    YARN_LOG_DIR=${HADOOP_LOG_DIR} \
    HADOOP_HDFS_HOME=${HADOOP_HOME} \
    HADOOP_YARN_HOME=${HADOOP_HOME} \
    HADOOP_MAPRED_HOME=${HADOOP_HOME} \
    HADOOP_COMMON_HOME=${HADOOP_HOME} \
    HADOOP_LIBEXEC_DIR=${HADOOP_HOME}/libexec \
    HADOOP_CONF_DIR=${HADOOP_HOME}/etc/hadoop \
    YARN_CONF_DIR=${HADOOP_HOME}/etc/hadoop \
    PATH=${PATH}:${HADOOP_HOME}/sbin:${HADOOP_HOME}/bin

COPY sources.list.debian /etc/apt/sources.list

# Prerequisites (SNAPPY)
# install libsnappy-dev
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends curl procps net-tools gosu \
  && rm -rf /var/lib/apt/lists/*

# Install hadoop
RUN useradd -md $HADOOP_HOME hadoop \
  && curl -O --progress https://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/hadoop-${HADOOP_VER}/hadoop-${HADOOP_VER}.tar.gz \
  && tar -xzf hadoop-${HADOOP_VER}.tar.gz -C $HADOOP_HOME --strip-components 1 \
  && mkdir $HADOOP_HOME/logs \
  && rm -f hadoop-${HADOOP_VER}.tar.gz

WORKDIR $HADOOP_HOME

VOLUME $HADOOP_HOME/logs $HADOOP_HOME/name $HADOOP_HOME/data

# Default Conf Files
COPY config/ /opt/hadoop-config

# NameNode ports
EXPOSE 50070 9000

# DataNode ports
EXPOSE 50075 50010 50020

# ResourceManager ports
EXPOSE 8030 8031 8032 8033 8088

# NodeManager ports
EXPOSE 8040 8042
