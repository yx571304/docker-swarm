    此镜像基于 alpine

### 构建镜像

    make build

### 上传镜像

> 修改 Makefile 文件中的前4行替换为自己的仓库地址及项目名称

    make push

### 测试(docker-compose)

> 创建数据目录

    mkdir -p /swarm/volumes/rsyslog

> 启动容器

    make start

### 验证

> 日志路径(日志名称 logging.driver.tag: )

    ls /swarm/volumes/rsyslog

### docker stack

> 创建数据目录

    mkdir -p /swarm/volumes/rsyslog

> 部署

    docker stack deploy -c rsyslog.yml cluster

> 查看运行状态

    docker service ls --filter name=cluster_rsyslog
    docker stack ps cluster --filter name=cluster_rsyslog

> 删除

    docker service rm $(docker service ls --filter name=cluster_rsyslog --format {{.Name}})


配置参考

https://docs.docker.com/config/containers/logging/log_tags/
